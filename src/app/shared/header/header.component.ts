import { Component, OnInit } from '@angular/core';
import { AuthComponent } from 'src/app/modals/auth/auth.component';
import { MatDialog } from '@angular/material/dialog';
import { AuthService } from 'src/app/services/auth.services';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import { IUser } from 'src/app/interfaces/user.interface';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  deviceInfo = null;
  user: IUser;
  isMobile: boolean;
  constructor(
    public dialog: MatDialog,
    private router: Router,
    private authService: AuthService,
    private userService: UserService,
    private deviceService: DeviceDetectorService

  ) {
    this.epicFunction();
  }

  getUserName(): any {
    const user = this.authService.getCurrentUser();
    return user ? user.email : '';
  }
  IsAdmin(): any {
    const user = this.authService.getCurrentUser();
    return user?.Roles === 'Admin';
  }



  ngOnInit(): void {
    this.userService.getUser().subscribe(
      res => {
        this.user = res;
      }
    );
  }

  getUserId(): any {
    const user = this.authService.getCurrentUser();
    this.router.navigate(
      ['/personal', user ? user.sub : '']);
  }

  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
  }

  openDialog(): void {
    this.dialog.open(AuthComponent);
  }

  logut(): void {
    this.user = null;
    this.authService.logout();
  }

  getSpinnerState(): Observable<boolean> {
    return this.authService.isAuth.asObservable();
  }

  isMenuActivate(status: string): boolean {
    return this.router.url.indexOf(status) !== -1;
  }

}
