import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { AuthGuard } from './guards/auth.guard';
import { AuthComponent } from './modals/auth/auth.component';
import { ItemsComponent } from './pages/items/items.component';
import { UsersComponent } from './pages/users/users.component';
import { RoomsComponent } from './pages/rooms/rooms.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { RolesComponent } from './pages/roles/roles.component';
import { AboutComponent } from './pages/about/about.component';
import { PersonalComponent } from './pages/personal/personal.component';
import { QrcodeComponent } from './modals/items/qrcode/qrcode.component';
import { QrComponent } from './pages/qr/qr.component';
import { LogsComponent } from './pages/logs/logs.component';




const routes: Routes = [
  { path: 'home', component: HomeComponent },
  {
    path: 'items', component: ItemsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'users', component: UsersComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'rooms', component: RoomsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'rooms', component: RoomsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'categories', component: CategoriesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'roles', component: RolesComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personal/:id', component: PersonalComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'qr/:id', component: QrComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'logs', component: LogsComponent,
    canActivate: [AuthGuard]
  },
  { path: 'about', component: AboutComponent },
  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports:
    [
      RouterModule.forRoot(routes),
    ],
  exports: [RouterModule]
})



export class AppRoutingModule { }
