
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';
import { IUser } from '../interfaces/user.interface';
import { retry } from 'rxjs/operators';
import { IData } from '../interfaces/data.interface';


@Injectable({
    providedIn: 'root'
})

export class UserService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private itemUrl = environment.apiUrl + 'user';
    constructor(private http: HttpClient) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }


    getUsers(): Observable<IUser[]> {
        return this.http.get<IUser[]>(`${this.itemUrl}`);
    }
    getUser(): Observable<IUser> {
        return this.http.get<IUser>(`${this.itemUrl}/person`);
    }

    deletItem(id: string) {
        return this.http.post(`${this.itemUrl}/deactivate/${id}`, id);
    }

    roleChange(id: string, role: number) {
        return this.http.post(`${this.itemUrl}/roleChange`, { id, role });
    }
    getUserById(id: string): Observable<IUser> {
        return this.http.get<IUser>(`${this.itemUrl}/${id}`);
    }
    changePassword(data) {
        return this.http.post(`${this.itemUrl}/changePassword/`, data);
    }
    changeName(data) {
        return this.http.post(`${this.itemUrl}/changeName/`, data);
    }
    getdata(data): Observable<IUser[]> {
        return this.http.post<IUser[]>(`${this.itemUrl}/getData/`, data);
    }
    getLengh(): Observable<number> {
        return this.http.get<number>(`${this.itemUrl}/getListSize`);
    }
    editUser(user) {
        return this.http.post(`${this.itemUrl}/edit/`, user);
    }
}

