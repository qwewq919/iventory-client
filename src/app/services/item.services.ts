
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';


@Injectable({
    providedIn: 'root'
})

export class ItemService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private itemUrl = environment.apiUrl + 'item';
    constructor(private http: HttpClient) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    getItemsUser(): Observable<IItem[]> {
        return this.http.get<IItem[]>(`${this.itemUrl}/UserItem`);
    }

    getAllItems(): Observable<IItem[]> {
        return this.http.get<IItem[]>(`${this.itemUrl}`);
    }

    deletItem(id: number) {
        return this.http.post(`${this.itemUrl}/deactivate`, id);
    }

    editItem(item: IItem): Observable<IItem> {
        return this.http.post<IItem>(`${this.itemUrl}/update`, item );
    }

    sortItem(item: any): Observable<IItem[]> {
        return this.http.post<IItem[]>(`${this.itemUrl}/sort`, item);
    }

    addItem(item: IItem) {
        return this.http.post<IItem>(`${this.itemUrl}/add`, item);
    }


    getdata(data): Observable<IItem[]> {
        return this.http.post<IItem[]>(`${this.itemUrl}/getData/`, data);
    }
    getLengh(): Observable<number> {
        return this.http.get<number>(`${this.itemUrl}/getListSize`);
    }
    getQrCode(id: string): Observable<any> {
        return this.http.get<any>(`${this.itemUrl}/getQR/${id}`);
    }

    getById(id: string): Observable<IItem> {
        return this.http.get<IItem>(`${this.itemUrl}/${id}`);
    }

    upload(id: number, formData) {
        return this.http.post(`${this.itemUrl}/Upload/${id}`, formData);
    }

    getImg(id: number): Observable<any> {
        return this.http.get<any>(`${this.itemUrl}/GetImg/${id}`);
    }

    toPrint(item: any[]): Observable<any[]> {
        return this.http.post<any>(`${this.itemUrl}/getPdf/`, { items: item });
    }

    userDelet(id: number): Observable<any> {
        return this.http.post<any>(`${this.itemUrl}/userDelet`, id);
    }

    DeletCategorie(id: number): Observable<any> {
        return this.http.post<any>(`${this.itemUrl}/DeletCategorie`, id);
    }
    deletRoom(id: number): Observable<any> {
        return this.http.post<any>(`${this.itemUrl}/DeletRoom`, id);
    }

}
