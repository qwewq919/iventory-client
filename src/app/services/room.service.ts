
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';
import { IRoom } from '../interfaces/room.interafe';


@Injectable({
    providedIn: 'root'
})

export class RoomService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private itemUrl = environment.apiUrl + 'rooms';
    constructor(private http: HttpClient) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }


    getAll(): Observable<IRoom[]> {
        return this.http.get<IRoom[]>(`${this.itemUrl}`);
    }


    editItem(room: IRoom): Observable<IRoom> {
        return this.http.post<IRoom>(`${this.itemUrl}/update`, room);
    }

    deActivate(id: string) {
        return this.http.get(`${this.itemUrl}/deactivate/${id}`);
    }

    addItem(room: IRoom){
        return this.http.post<IItem>(`${this.itemUrl}/add`, room);
    }

    getdata(data): Observable<IRoom[]> {
        return this.http.post<IRoom[]>(`${this.itemUrl}/getData/`, data);
    }
    getLengh(): Observable<number> {
        return this.http.get<number>(`${this.itemUrl}/getListSize`);
    }

}
