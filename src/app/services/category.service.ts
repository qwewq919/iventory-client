
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';
import { ICategory } from '../interfaces/category.interface';


@Injectable({
    providedIn: 'root'
})

export class CategoryService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private itemUrl = environment.apiUrl + 'category';
    constructor(private http: HttpClient) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    getAll(): Observable<ICategory[]> {
        return this.http.get<ICategory[]>(`${this.itemUrl}`);
    }


    editItem(category: ICategory): Observable<ICategory> {
        return this.http.post<ICategory>(`${this.itemUrl}/update`, category);
    }


    deActivate(id: string) {
        return this.http.get(`${this.itemUrl}/deactivate/${id}`);
    }


    addItem(category: ICategory) {
        return this.http.post<ICategory>(`${this.itemUrl}/add`, category);
    }

    getdata(data): Observable<ICategory[]> {
        return this.http.post<ICategory[]>(`${this.itemUrl}/getData/`, data);
    }
    getLengh(): Observable<number> {
        return this.http.get<number>(`${this.itemUrl}/getListSize`);
    }

}
