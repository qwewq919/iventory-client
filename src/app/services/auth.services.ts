
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/user.interface';
import { HttpClient } from '@angular/common/http';
import { takeUntil, tap } from 'rxjs/operators';
import { StorageService } from '../intenseptor/storage.service';
import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})

export class AuthService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private authUrl = environment.apiUrl + 'auth';
    isAuth: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    constructor(
        private router: Router,
        private http: HttpClient,
        private storage: StorageService
    ) {
        const token = localStorage.getItem('access_token');
        if (token) {
            this.isAuth.next(true);
        }
    }

    getCurrentUser(): IUser {
        const token = localStorage.getItem('access_token');
        if (!token) {
            return null;
        }
        return JSON.parse(atob(token.split('.')[1]));
    }

    logout() {
        this.storage.clear();
        this.router.navigate(['login']);
        this.isAuth.next(false);
    }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    login(authData: Partial<IUser>) {
        const url = `${this.authUrl}/login`;
        return this.http
            .post<any>(url, authData)
            .pipe(
                takeUntil(this.unsubscribe),
                tap(data => {
                    console.log(data)
                    if (data.access_token) {
                        this.isAuth.next(true);
                        this.storage.set('access_token', data.access_token);
                    }
                })
            );
    }

    reg(data: IUser) {
        return this.http.post<any>(`${this.authUrl}/register`, data);
    }
}
