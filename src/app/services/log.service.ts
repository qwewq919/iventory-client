
import { Injectable, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { IItem } from '../interfaces/item.interface';
import { IRoom } from '../interfaces/room.interafe';
import { ILogs } from '../interfaces/log.interface';


@Injectable({
    providedIn: 'root'
})

export class LogService implements OnDestroy {
    private unsubscribe: Subject<any> = new Subject<any>();
    private itemUrl = environment.apiUrl + 'logs';
    constructor(private http: HttpClient) { }

    ngOnDestroy(): void {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }


    getdata(data): Observable<ILogs[]> {
        return this.http.post<ILogs[]>(`${this.itemUrl}/getData/`, data);
    }

    getLengh(): Observable<number> {
        return this.http.get<number>(`${this.itemUrl}/getListSize`);
    }
    sortItem(item: any): Observable<ILogs[]> {
        console.log(item)
        return this.http.post<ILogs[]>(`${this.itemUrl}/sort`, item);
    }
}
