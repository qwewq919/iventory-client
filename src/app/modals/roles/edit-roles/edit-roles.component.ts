import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Roles } from 'src/app/interfaces/roles.interface';
import { ToastService } from 'src/app/services/toast.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit-roles',
  templateUrl: './edit-roles.component.html',
  styleUrls: ['./edit-roles.component.css']
})
export class EditRolesComponent implements OnInit {

  roleUser: FormControl;
  roles: Roles[] = [
    { value: 0, viewValue: 'No roles' },
    { value: 1, viewValue: 'User' },
    { value: 2, viewValue: 'Admin' }
  ];
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    private userService: UserService,
    private toast: ToastService,
    private fb: FormBuilder,

  ) {
    this.formBuild();
  }

  ngOnInit(): void {
    this.roleUser.setValue(this.data.roles);
  }

  formBuild() {
    this.roleUser = new FormControl('');
  }

  roleChange() {
    this.userService.roleChange(this.data.id, this.roleUser.value).subscribe(res => {
      this.toast.openSnackBar('Changes saved');
    });
  }

}
