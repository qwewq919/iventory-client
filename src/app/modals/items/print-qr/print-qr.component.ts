import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
  selector: 'app-print-qr',
  templateUrl: './print-qr.component.html',
  styleUrls: ['./print-qr.component.css']
})
export class PrintQRComponent implements OnInit {

  qrToPrint: any[] = [];

  public qr(qrcode): string {
    return `data:image/gif;base64,${qrcode}`;
  }

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    this.qrToPrint = this.data;
  }

}
