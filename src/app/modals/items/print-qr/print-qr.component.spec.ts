import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintQRComponent } from './print-qr.component';

describe('PrintQRComponent', () => {
  let component: PrintQRComponent;
  let fixture: ComponentFixture<PrintQRComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PrintQRComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintQRComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
