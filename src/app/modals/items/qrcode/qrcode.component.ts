import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { map } from 'rxjs/operators';
import { ItemService } from 'src/app/services/item.services';

@Component({
  selector: 'app-qrcode',
  templateUrl: './qrcode.component.html',
  styleUrls: ['./qrcode.component.css']
})
export class QrcodeComponent implements OnInit {
  id: string;
  isLoading = true;
  imgB64: string;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: string,
    private itemService: ItemService,
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  public get qr(): string {
    return `data:image/gif;base64,${this.imgB64}`;
  }

  async loadData(): Promise<any> {
    this.id = this.data;
    await this.loadQRCode().toPromise();
    this.isLoading = false;
  }

  loadQRCode() {
    return this.itemService.getQrCode(this.id).pipe(
      map(
        res => {
          this.imgB64 = res;
        }
      )
    );
  }

}
