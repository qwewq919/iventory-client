import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IItem } from 'src/app/interfaces/item.interface';
import { ItemService } from 'src/app/services/item.services';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  itemForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.formBuilder();
  }

  private formBuilder() {
    this.itemForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }
  addItem() {
    this.itemService.addItem(this.itemForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Added new item');
      }
    );
  }

}
