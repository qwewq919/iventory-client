import { Component, OnInit, Inject } from '@angular/core';
import { IItem } from 'src/app/interfaces/item.interface';
import { ItemService } from 'src/app/services/item.services';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IUser } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { ICategory } from 'src/app/interfaces/category.interface';
import { CategoryService } from 'src/app/services/category.service';
import { RoomService } from 'src/app/services/room.service';
import { map } from 'rxjs/operators';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  itemForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IItem,
    private fb: FormBuilder,
    private itemService: ItemService,
    private userService: UserService,
    private categoryService: CategoryService,
    private roomsService: RoomService,
    private toast: ToastService
  ) { }
  // IsClearU = false;
  // IsClearC = false;
  // IsClearR = false;
  item: IItem;
  property: string[] = [];
  users: IUser[] = [];
  rooms: IRoom[] = [];
  categories: ICategory[] = [];

  ngOnInit(): void {

    this.formBuild();
    this.itemForm.setValue({
      id: this.data.id,
      name: this.data.name,
      accountId: this.data.account?.id || null,
      categoryId: this.data.category?.id || null,
      roomId: this.data.room?.id || null,
    });
    this.loadData();
    // this.itemForm.get('accountId').valueChanges.subscribe(
    //   res => {
    //     if (this.property.indexOf('accountId') === -1) {
    //       this.property.push('accountId');
    //     }
    //   }
    // );
    // this.itemForm.get('categoryId').valueChanges.subscribe(
    //   res => {
    //     if (this.property.indexOf('categoryId') === -1) {
    //       this.property.push('categoryId');
    //     }
    //   }
    // );
    // this.itemForm.get('roomId').valueChanges.subscribe(
    //   res => {
    //     if (this.property.indexOf('roomId') === -1) {
    //       this.property.push('roomId');
    //     }
    //   }
    // );
  }

  async changeItem() {
    this.itemService.editItem(this.itemForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Changes saved');
      }
    );
    // this.IsClearU = false;
    // this.IsClearC = false;
    // this.IsClearR = false;
  }

  async loadData(): Promise<any> {
    await this.loadRooms().toPromise();
    await this.loadUsers().toPromise();
    await this.loadCategory().toPromise();
  }

  userClear() {

    this.itemForm.patchValue({
      accountId: null,
    });
    // this.IsClearU = true;
  }
  categoryClear() {
    this.itemForm.patchValue({
      categoryId: null,
    });

    // this.IsClearC = true;
  }
  roomClear() {
    this.itemForm.patchValue({
      roomId: null,
    });
    // this.IsClearR = true;
  }

  userDelet() {
    const id = this.data?.id;
    return this.itemService.userDelet(id)
      .pipe(
        map(
          () => { }
        )
      );
  }


  deletCategorie() {
    const id = this.data?.id;
    return this.itemService.DeletCategorie(id)
      .pipe(
        map(
          () => { }
        )
      );
  }

  deletRoom() {
    const id = this.data?.id;
    return this.itemService.deletRoom(id)
      .pipe(
        map(
          () => { }
        )
      );
  }


  loadUsers() {
    return this.userService.getUsers()
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadCategory() {
    return this.categoryService.getAll()
      .pipe(
        map(
          res => { this.categories = res; }
        ));
  }

  loadRooms() {
    return this.roomsService.getAll()
      .pipe(
        map(
          res => {
            this.rooms = res;
          }
        )
      );
  }

  formBuild() {
    this.itemForm = this.fb.group({
      id: [''],
      name: ['' , Validators.compose([Validators.required, Validators.minLength(1)])],
      accountId: [''],
      categoryId: [''],
      roomId: ['']
    });
  }
}
