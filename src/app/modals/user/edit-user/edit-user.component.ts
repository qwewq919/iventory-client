import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IUser } from 'src/app/interfaces/user.interface';
import { CategoryService } from 'src/app/services/category.service';
import { ToastService } from 'src/app/services/toast.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
  styleUrls: ['./edit-user.component.css']
})
export class EditUserComponent implements OnInit {

  itemForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IUser,
    private categoryService: CategoryService,
    private toast: ToastService,
    public dialog: MatDialog,
    private fb: FormBuilder,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.formBuild();
    this.itemForm.setValue({
      id: this.data.id,
      oldEmail: this.data.email,
      email: this.data.email,
      firstName: this.data.firstName,
      lastName: this.data.lastName
    });
  }


  formBuild() {
    this.itemForm = this.fb.group({
      id: [''],
      oldEmail: [''],
      email: ['' ,  Validators.compose([Validators.required, Validators.minLength(1)])],
      firstName: ['' , Validators.compose([Validators.required, Validators.minLength(1)])],
      lastName: ['' , Validators.compose([Validators.required, Validators.minLength(1)])]
    });
  }

  changeItem(){
    this.userService.editUser(this.itemForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Changes saved');
      }
    );
  }
}
