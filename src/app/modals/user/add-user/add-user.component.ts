import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AuthInterceptor } from 'src/app/intenseptor/auth.interceptor';
import { AuthService } from 'src/app/services/auth.services';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  loginForm: FormGroup;
  step = true;
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    public dialog: MatDialog,
    public toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.formBuild();
  }


  reg(): void {
    this.authService.reg(this.loginForm.value).subscribe(res => {
      this.toast.openSnackBar('Added new user');
    });
  }


  private formBuild(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      firstName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
      lastName: ['', Validators.compose([Validators.required, Validators.minLength(2)])],
    });
  }
}

