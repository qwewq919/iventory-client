import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CategoryService } from 'src/app/services/category.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {

  categoryForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toast: ToastService,
    private categoryService: CategoryService,
  ) { }

  ngOnInit(): void {
    this.formBuilder();
  }

  private formBuilder() {
    this.categoryForm = this.fb.group({
      name: ['', Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }

  addCategory() {
    this.categoryService.addItem(this.categoryForm.value).subscribe(res =>
      this.toast.openSnackBar('Added new category')
    );
  }

}
