import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ICategory } from 'src/app/interfaces/category.interface';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { CategoryService } from 'src/app/services/category.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-eddit-category',
  templateUrl: './eddit-category.component.html',
  styleUrls: ['./eddit-category.component.css']
})
export class EdditCategoryComponent implements OnInit {

  categoryForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: ICategory,
    private fb: FormBuilder,
    private toast: ToastService,
    private categoryService: CategoryService,
  ) { }
  category: ICategory;

  ngOnInit(): void {
    this.formBuild();
    this.categoryForm.setValue({
      id: this.data.id,
      name: this.data.name,
    });
  }

  private formBuild() {
    this.categoryForm = this.fb.group({
      id: [''],
      name: ['',Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }

  changeItem() {
    this.categoryService.editItem(this.categoryForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Changes saved');
      }
    );
  }

}
