import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdditCategoryComponent } from './eddit-category.component';

describe('EdditCategoryComponent', () => {
  let component: EdditCategoryComponent;
  let fixture: ComponentFixture<EdditCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdditCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdditCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
