import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.services';
import { AuthInterceptor } from 'src/app/intenseptor/auth.interceptor';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  step = true;
  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private toastr: ToastrService,
    private authInteseptor: AuthInterceptor
  ) { }

  ngOnInit(): void {
    this.formBuild();
  }

  login(): void {
    this.authService.login(this.loginForm.value)
      .subscribe((req: { access_token: string }) => {
        localStorage.setItem('access_token', req.access_token);
        this.toastr.success('Hello');
      }, err => {
        this.showSuccess(err.error.message);
      }
      );
  }

  showSuccess(error) {
    this.toastr.error(error);
  }

  reg(): void {
    this.authService.reg(this.loginForm.value).subscribe(req =>{
      this.toastr.success('Account registration complete');
    }, err => {
      this.showSuccess(err.error.message);
    });
  }

  changeWrapper(): void {
    this.authService.logout();
    this.step = !this.step;
  }

  private formBuild(): void {
    this.loginForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      firstName: [''],
      lastName: [''],
    });
  }
}
