import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RoomService } from 'src/app/services/room.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.component.html',
  styleUrls: ['./add-room.component.css']
})
export class AddRoomComponent implements OnInit {

  roomForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private roomService: RoomService,
    private toast: ToastService
  ) { }

  ngOnInit(): void {
    this.formBuilder();
  }

  private formBuilder() {
    this.roomForm = this.fb.group({
      name: ['' ,  Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }

  addRoom() {
    this.roomService.addItem(this.roomForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Added new room');
      }
    );
  }

}
