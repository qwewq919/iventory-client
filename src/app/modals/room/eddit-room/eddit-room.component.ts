import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IItem } from 'src/app/interfaces/item.interface';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { ItemService } from 'src/app/services/item.services';
import { RoomService } from 'src/app/services/room.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-eddit-room',
  templateUrl: './eddit-room.component.html',
  styleUrls: ['./eddit-room.component.css']
})
export class EdditRoomComponent implements OnInit {

  roomForm: FormGroup;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: IRoom,
    private fb: FormBuilder,
    private toast: ToastService,
    private roomService: RoomService,
  ) { }
  room: IRoom;

  ngOnInit(): void {
    this.formBuild();
    this.roomForm.setValue({
      id: this.data.id,
      name: this.data.name,
    });
  }

  private formBuild() {
    this.roomForm = this.fb.group({
      id: [''],
      name: ['' ,  Validators.compose([Validators.required, Validators.minLength(1)])],
    });
  }

  changeItem() {
    this.roomService.editItem(this.roomForm.value).subscribe(
      res => {
        this.toast.openSnackBar('Changes saved');
      }
    );
  }

}
