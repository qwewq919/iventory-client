import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EdditRoomComponent } from './eddit-room.component';

describe('EdditRoomComponent', () => {
  let component: EdditRoomComponent;
  let fixture: ComponentFixture<EdditRoomComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EdditRoomComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EdditRoomComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
