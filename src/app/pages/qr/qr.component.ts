import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { ICategory } from 'src/app/interfaces/category.interface';
import { IItem } from 'src/app/interfaces/item.interface';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { IUser } from 'src/app/interfaces/user.interface';
import { CategoryService } from 'src/app/services/category.service';
import { ItemService } from 'src/app/services/item.services';
import { RoomService } from 'src/app/services/room.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.scss']
})
export class QrComponent implements OnInit {
  id: string;
  public progress: number;
  public message: string;
  @Output() public onUploadFinished = new EventEmitter();
  item: IItem;
  emptyImgSrc = '../../../assets/img/items/emptyImg.jpg';
  itemForm: FormGroup;
  imgB64;
  isLoading = true;
  formData = new FormData();
  users: IUser[] = [];
  rooms: IRoom[] = [];
  categories: ICategory[] = [];
  constructor(
    private http: HttpClient,
    private route: ActivatedRoute,
    private itemService: ItemService,
    private fb: FormBuilder,
    private userService: UserService,
    private categoryService: CategoryService,
    private roomsService: RoomService
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.formBuild();
  }

  async loadData(): Promise<any> {
    await this.loadItem().toPromise();
    await this.loadRooms().toPromise();
    await this.loadUsers().toPromise();
    await this.loadCategory().toPromise();
    await this.loadImg().toPromise()
      .then(() => { })
      .catch((error) => {
        const errorSub = throwError(error);
        errorSub.subscribe();
      });
  }



  // changeItem() {
  //   console.log(this.itemForm.value)
  //   this.itemService.editItem(this.itemForm.value).subscribe(
  //     res => {
  //       // this.loadData();
  //     }
  //   );

  // }

  loadUsers() {
    return this.userService.getUsers()
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadImg() {
    const ids = this.route.snapshot.params.id;
    return this.itemService.getImg(ids)
      .pipe(
        map(
          res => {
            this.imgB64 = res.imageString;
          }
        ));
  }

  loadCategory() {
    return this.categoryService.getAll()
      .pipe(
        map(
          res => { this.categories = res; }
        ));
  }

  loadRooms() {
    return this.roomsService.getAll()
      .pipe(
        map(
          res => {
            this.rooms = res;
          }
        )
      );
  }

  loadItem() {
    this.id = this.route.snapshot.params.id;
    return this.itemService.getById(this.id).pipe(
      map(
        res => {
          this.item = res;
          this.itemForm.setValue({
            id: this.item?.id,
            name: this.item?.name,
            accountId: this.item?.account?.id || null,
            categoryId: this.item?.category?.id || null,
            roomId: this.item?.room?.id || null,
          });
          this.isLoading = false;
        }
      )
    );
  }

  formBuild(): void {
    this.itemForm = this.fb.group({
      id: [''],
      name: [''],
      accountId: [''],
      categoryId: [''],
      roomId: ['']
    });
  }

  public uploadFile(files) {
    if (files.length === 0) {
      return;
    }
    const id = this.item.id;
    let fileToUpload = <File>files[0];
    this.formData.append(fileToUpload.name, fileToUpload);
    this.itemService.upload(id, this.formData).subscribe(
      res => {
        this.loadData();
      });

  }
}


