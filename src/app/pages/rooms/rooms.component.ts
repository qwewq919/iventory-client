import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { IData } from 'src/app/interfaces/data.interface';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { IUser } from 'src/app/interfaces/user.interface';
import { DeletComponent } from 'src/app/modals/delet/delet.component';
import { AddRoomComponent } from 'src/app/modals/room/add-room/add-room.component';
import { EdditRoomComponent } from 'src/app/modals/room/eddit-room/eddit-room.component';

import { RoomService } from 'src/app/services/room.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.scss']
})
export class RoomsComponent implements OnInit {

  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  pageIndex: number;
  pageSize: number;
  length: number;
  isLoading = true;
  rooms: IRoom[] = [];
  constructor(
    private roomService: RoomService,
    private toast: ToastService,
    public dialog: MatDialog) { }


  ngOnInit(): void {
    this.loadData();
  }

  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.loadRoom().toPromise();
    this.isLoading = false;
  }

  loadRoom() {
    return this.roomService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.rooms = res;
          }
        )
      );
  }

  loadLengh() {
    return this.roomService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }
  // loadData() {
  //   this.roomService.getLengh().subscribe(res => {
  //     this.length = res;
  //   });

  //   this.dataFirst.pageIndex = 0;
  //   this.dataFirst.pageSize = 5;
  //   this.roomService.getdata(this.dataFirst).subscribe(res => {
  //     this.rooms = res;
  //   });
  // }

  deleteActive(room: IRoom, i) {
    const dialogRef = this.dialog.open(DeletComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.rooms.splice(i, 1);
        this.length = this.length - 1;
        this.roomService.deActivate(room.id).subscribe(res => {
          this.toast.openSnackBar('Room is deleted ');
        });
      }
    });
  }

  openDialogAdd(): void {

    const dialogRef = this.dialog.open(AddRoomComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  public getServerData(event?: PageEvent) {
    this.roomService.getdata(event).subscribe(res => {
      this.rooms = res;
    });
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(EdditRoomComponent, {
      width: '250px',
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }
}
