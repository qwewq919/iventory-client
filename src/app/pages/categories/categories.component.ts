import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { ICategory } from 'src/app/interfaces/category.interface';
import { IData } from 'src/app/interfaces/data.interface';
import { AddCategoryComponent } from 'src/app/modals/category/add-category/add-category.component';
import { EdditCategoryComponent } from 'src/app/modals/category/eddit-category/eddit-category.component';
import { DeletComponent } from 'src/app/modals/delet/delet.component';

import { CategoryService } from 'src/app/services/category.service';
import { ToastService } from 'src/app/services/toast.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  pageIndex: number;
  pageSize: number;
  length: number;
  isLoading = true;
  categories: ICategory[] = [];
  constructor(
    private categoryService: CategoryService,
    private toast: ToastService,
    public dialog: MatDialog,
  ) { }


  ngOnInit(): void {
    this.loadData();
  }


  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.loadCategory().toPromise();
    this.isLoading = false;
  }

  loadCategory() {
    return this.categoryService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.categories = res;
          }
        )
      );
  }

  loadLengh() {
    return this.categoryService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }

  deleteActive(category: ICategory, i) {
    const dialogRef = this.dialog.open(DeletComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.categories.splice(i, 1);
        this.length = this.length - 1;
        this.categoryService.deActivate(category.id).subscribe(res => {
          this.toast.openSnackBar('Category is deleted ');
        });
      }
    });
  }

  openDialogAdd(): void {

    const dialogRef = this.dialog.open(AddCategoryComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(EdditCategoryComponent, {
      width: '250px',
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  public getServerData(event?: PageEvent) {
    this.categoryService.getdata(event).subscribe(res => {
      this.categories = res;
    });
  }

}
