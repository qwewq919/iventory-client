import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/interfaces/user.interface';
import { ToastService } from 'src/app/services/toast.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {
  id: string;
  user: IUser;
  isLoading = true;
  userForm: FormGroup;
  passwordForm: FormGroup;
  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private toast: ToastService,
  ) { }

  ngOnInit(): void {
    this.loadData();
    this.formBuild();
  }

  loadData() {
    this.id = this.route.snapshot.params.id;
    this.userService.getUserById(this.id)
      .subscribe(res => {
        this.user = res;
        this.userForm.setValue({
          id: this.user.id,
          lastName: this.user.lastName,
          firstName: this.user.firstName,
        });
        this.passwordForm.setValue({
          id: this.user.id,
          password: [''],
          newPassword: ['']
        });
        this.isLoading = false;
      });

  }
  savePassword() {
    this.userService.changePassword(this.passwordForm.value).subscribe(res => {
      this.toast.openSnackBar('password is changet');
    });
  }
  saveName() {
    this.userService.changeName(this.userForm.value).subscribe(res => { });
  }

  private formBuild() {
    this.userForm = this.fb.group({
      id: [''],
      lastName: [''],
      firstName: [''],
    });
    this.passwordForm = this.fb.group({
      id: [''],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    });
  }

}
