import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ItemService } from 'src/app/services/item.services';
import { IItem } from 'src/app/interfaces/item.interface';
import { MatDialog } from '@angular/material/dialog';

import { RoomService } from 'src/app/services/room.service';
import { IRoom } from 'src/app/interfaces/room.interafe';
import { combineLatest } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';

import { IUser } from 'src/app/interfaces/user.interface';
import { UserService } from 'src/app/services/user.service';
import { map } from 'rxjs/operators';
import { CategoryService } from 'src/app/services/category.service';
import { ICategory } from 'src/app/interfaces/category.interface';
import { AddItemComponent } from 'src/app/modals/items/add-item/add-item.component';
import { EditComponent } from 'src/app/modals/items/edit/edit.component';
import { PageEvent } from '@angular/material/paginator';
import { IData } from 'src/app/interfaces/data.interface';
import { DeviceDetectorService } from 'ngx-device-detector';
import { QrcodeComponent } from 'src/app/modals/items/qrcode/qrcode.component';
import { Router } from '@angular/router';
import { PrintQRComponent } from 'src/app/modals/items/print-qr/print-qr.component';
import { element } from 'protractor';
import { ToastService } from 'src/app/services/toast.service';
import { DeletComponent } from 'src/app/modals/delet/delet.component';


@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.scss']
})
export class ItemsComponent implements OnInit {
  items: IItem[] = [];
  qrToPrint: any[] = [];
  isGrid = false;
  searchGroup: FormGroup;
  idForPdf: number[] = [];
  isVisCheck = false;
  rooms: IRoom[] = [];
  users: IUser[] = [];
  categories: ICategory[] = [];
  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  deviceInfo = null;
  isLoading = true;
  isMobile: boolean;
  constructor(
    private fb: FormBuilder,
    private itemService: ItemService,
    public dialog: MatDialog,
    private roomsService: RoomService,
    private userService: UserService,
    private categoryService: CategoryService,
    private deviceService: DeviceDetectorService,
    private toast: ToastService,
    private router: Router,
  ) {
    this.epicFunction();
  }

  get selectedLength(): number {
    return this.items.filter(item => item.selected).length;
  }

  ngOnInit(): void {
    this.loadData();
    this.formBuild();
  }

  setAll(event) {
    if (event) {
      this.items.forEach(item => item.selected = true);
    }
    else {
      this.items.forEach(item => item.selected = false);
      // this.idForPdf = [];
    }
  }

  toPdf() {
    const idForPdf = this.items.filter(item => item.selected).map(item => item.id);

    this.itemService.toPrint(idForPdf).subscribe(
      res => {
        this.qrToPrint = res;
        this.openDialogPrint(this.qrToPrint);
      }
    );
  }

  addOnPdf(event, id: number) {
    if (event) {
      // this.idForPdf.push(id);
      const index = this.items.findIndex(el => el.id === id);
      this.items[index].selected = event;
    }
    else {
      const indexto = this.items.findIndex(el => el.id === id);
      this.items[indexto].selected = event;
      // const index = this.idForPdf.indexOf(id);
      // this.idForPdf.splice(index, 1);
    }
  }

  IsCheck(event) {
    if (event) {
      this.isVisCheck = event;
    }
    else {
      this.isVisCheck = event;
      // this.idForPdf = [];
    }
  }

  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
  }

  openDialogAdd(): void {

    const dialogRef = this.dialog.open(AddItemComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(EditComponent, {
      width: '450px',
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  openDialogPrint(qrToPrint): void {

    const dialogRef = this.dialog.open(PrintQRComponent, {
      width: '100%',
      height: '100%',
      data: qrToPrint
    });
  }

  openDialogQR(id): void {
    const dialogRef = this.dialog.open(QrcodeComponent, {
      width: '450px',
      data: id,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  private formBuild() {
    this.searchGroup = this.fb.group({
      itemName: [''],
      user: [''],
      category: [''],
      room: [''],
      items: null,
    });
  }

  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.getData().toPromise();
    // await this.loadItems().toPromise();
    await this.loadRooms().toPromise();
    await this.loadUsers().toPromise();
    await this.loadCategory().toPromise();
    this.isLoading = false;
  }

  getData() {
    return this.itemService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.items = res;
          }
        )
      );
  }

  getServerData(event?: PageEvent) {
    this.itemService.getdata(event).subscribe(res => {
      this.items = res;
    });
  }

  loadUsers() {
    return this.userService.getUsers()
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadCategory() {
    return this.categoryService.getAll()
      .pipe(map(
        res => { this.categories = res; }
      ));
  }

  loadRooms() {
    return this.roomsService.getAll()
      .pipe(
        map(
          res => {
            this.rooms = res;
          }
        )
      );
  }

  loadItems() {
    return this.itemService.getAllItems()
      .pipe(
        map(res => {
          this.items = res;
        }));
  }

  loadLengh() {
    return this.itemService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }

  toItem(id: number) {
    this.router.navigate([`qr/${id}`]);
  }

  sort() {
    console.log((this.searchGroup.value));

    this.isLoading = true;
    this.itemService.sortItem(this.searchGroup.value).subscribe(
      res => {
        this.items = res;
        this.isLoading = false;
      }
    );
  }

  clearSort() {
    this.formBuild();
    this.loadData();
  }

  changeList(): void {
    this.isGrid = !this.isGrid;
  }

  deleteActive(item: IItem, i) {
    const dialogRef = this.dialog.open(DeletComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.items.splice(i, 1);
        this.length = this.length - 1;
        this.itemService.deletItem(item.id).subscribe(res => {
          this.toast.openSnackBar('User is deleted ');
        });
      }
    });
  }

  isGridActive() {
    this.isGrid = true;
  }

  isGridDeactive() {
    this.isGrid = false;
  }

}
