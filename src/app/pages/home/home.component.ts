import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AuthComponent } from 'src/app/modals/auth/auth.component';
import { AddItemComponent } from 'src/app/modals/items/add-item/add-item.component';
import { AddUserComponent } from 'src/app/modals/user/add-user/add-user.component';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  deviceInfo = null;
  isMobile: boolean;
  constructor(
    public dialog: MatDialog,
    private deviceService: DeviceDetectorService
  ) {
    this.epicFunction();
  }

  ngOnInit(): void {
  }
  epicFunction() {
    this.deviceInfo = this.deviceService.getDeviceInfo();
    this.isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();
  }
  openDialog(): void {
    this.dialog.open(AuthComponent);
  }
  openDialogAdd(): void {

    const dialogRef = this.dialog.open(AddItemComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  openDialogAddUser(): void {

    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

}
