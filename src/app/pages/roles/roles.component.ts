import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { IData } from 'src/app/interfaces/data.interface';
import { IUser } from 'src/app/interfaces/user.interface';
import { EditRolesComponent } from 'src/app/modals/roles/edit-roles/edit-roles.component';

import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent implements OnInit {
  users: IUser[] = [];
  isLoading = true;
  statuses = {
    2: 'Admin',
    1: 'User',
    0: 'No roles',
  };
  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  constructor(
    private userService: UserService,
    public dialog: MatDialog
  ) { }

  ngOnInit(): void {
    this.loadData();
  }

  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.loadUsers().toPromise();
    this.isLoading = false;
  }

  loadUsers() {
    return this.userService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadLengh() {
    return this.userService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }

  openDialog(item): void {
    const dialogRef = this.dialog.open(EditRolesComponent, {
      width: '450px',
      data: item,
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  public getServerData(event?: PageEvent) {
    this.userService.getdata(event).subscribe(res => {
      this.users = res;
    });
  }

}
