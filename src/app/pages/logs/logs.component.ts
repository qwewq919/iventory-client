import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { IData } from 'src/app/interfaces/data.interface';
import { IItem } from 'src/app/interfaces/item.interface';
import { ILogs } from 'src/app/interfaces/log.interface';
import { IUser } from 'src/app/interfaces/user.interface';
import { ItemService } from 'src/app/services/item.services';
import { LogService } from 'src/app/services/log.service';
import { RoomService } from 'src/app/services/room.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent implements OnInit {

  status = [
    // Created = 1,
    // Updated = 2,
    // Deleted = 4,
    { view: 'Created', value: 'Created' },
    { view: 'Updated', value: 'Updated' },
    { view: 'Deleted', value: 'Deleted' },
    { view: 'Withdrawn', value: 'Withdrawn' },
    { view: 'Uncategorized', value: 'Uncategorized' },
    { view: 'Eviction', value: 'Eviction' },
    { view: 'UserChange', value: 'UserChange' },
    { view: 'RoomChange', value: 'RoomChange' },
    { view: 'CategoryChange', value: 'CategoryChange' },
    { view: 'AddUser', value: 'AddUser' },
    { view: 'AddRoom', value: 'AddRoom' },
    { view: 'AddCategory', value: 'AddCategory' },

  ];
  items: IItem[] = [];
  searchGroup: FormGroup;
  users: IUser[] = [];
  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  statuses = {
    2: 'Updated',
    1: 'Created',
    4: 'Deleted',
    8: 'Withdrawn',
    16: 'Uncategorized',
    32: 'Eviction',
    64: 'User Change',
    128: 'Room Change',
    256: 'Category Change',
    512: 'Add User',
    1024: 'Add Room',
    2048: 'Add Category'
  };
  pageIndex: number;
  pageSize: number;
  length: number;
  isLoading = true;
  logs: ILogs[] = [];
  constructor(
    private itemService: ItemService,
    private logService: LogService,
    private userService: UserService,
    private fb: FormBuilder,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.formBuild();
    this.loadData();
  }

  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.loadLogs().toPromise();
    await this.loadUsers().toPromise();
    await this.loadItems().toPromise();
    this.isLoading = false;
  }

  public getServerData(event?: PageEvent) {
    this.logService.getdata(event).subscribe(res => {

      this.logs = res;
    });
  }

  loadLogs() {
    return this.logService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.logs = res;
          }
        )
      );
  }

  loadItems() {
    return this.itemService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.items = res;
          }
        )
      );
  }

  loadUsers() {
    return this.userService.getUsers()
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadLengh() {
    return this.logService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }

  sort() {
    this.isLoading = true;
    this.logService.sortItem(this.searchGroup.value).subscribe(
      res => {
        this.logs = res;
        this.isLoading = false;
      }
    );
  }

  clearSort() {
    this.formBuild();
    this.loadData();
  }

  private formBuild() {
    this.searchGroup = this.fb.group({
      status: [''],
      user: [''],
      items: [''],
    });
  }
}
