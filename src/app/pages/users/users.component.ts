import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { PageEvent } from '@angular/material/paginator';
import { map } from 'rxjs/operators';
import { IData } from 'src/app/interfaces/data.interface';
import { IUser } from 'src/app/interfaces/user.interface';
import { DeletComponent } from 'src/app/modals/delet/delet.component';
import { AddUserComponent } from 'src/app/modals/user/add-user/add-user.component';
import { EditUserComponent } from 'src/app/modals/user/edit-user/edit-user.component';
import { AuthService } from 'src/app/services/auth.services';
import { ToastService } from 'src/app/services/toast.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  users: IUser[] = [];
  statuses = {
    2: 'Admin',
    1: 'User',
    0: 'No roles',
  };
  dataFirst: IData = {
    pageIndex: 0,
    pageSize: 5
  };
  pageEvent: PageEvent;
  pageIndex: number;
  pageSize: number;
  length: number;
  isLoading = true;
  constructor(
    private userService: UserService,
    private toast: ToastService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loadData();
  }

  openDialogAdd(): void {
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '250px',
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }

  openDialogEdit(item): void {
    const dialogRef = this.dialog.open(EditUserComponent, {
      width: '250px',
      data: item,
    });
    dialogRef.afterClosed().subscribe(result => {
      this.loadData();
    });
  }


  async loadData(): Promise<any> {
    await this.loadLengh().toPromise();
    await this.loadUsers().toPromise();
    this.isLoading = false;
  }

  loadUsers() {
    return this.userService.getdata(this.dataFirst)
      .pipe(
        map(
          res => {
            this.users = res;
          }
        )
      );
  }

  loadLengh() {
    return this.userService.getLengh()
      .pipe(
        map(
          res => this.length = res
        )
      );
  }

  deleteActive(item: IUser, i) {
    const dialogRef = this.dialog.open(DeletComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.users.splice(i, 1);
        this.length = this.length - 1;
        this.userService.deletItem(item.id).subscribe(res => {
          this.toast.openSnackBar('User is deleted ');
        });
      }
    });
  }

  public getServerData(event?: PageEvent) {
    this.userService.getdata(event).subscribe(res => {
      this.users = res;
    });
  }
}


