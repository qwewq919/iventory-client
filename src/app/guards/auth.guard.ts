import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { StorageService } from '../intenseptor/storage.service';
import { AuthService } from '../services/auth.services';




@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(
    private router: Router,
    private storage: StorageService,
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {


    const token = localStorage.getItem('access_token');
    if (token && token !== 'undefined' && token !== 'null') {
      return true;
    }

    this.router.navigate(['home']);

    return false;
  }
}
