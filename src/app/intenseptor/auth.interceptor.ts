import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { StorageService } from './storage.service';


@Injectable(
  {
    providedIn: 'root'
  }
)
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private storage: StorageService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let headers = req.headers;
    const token = this.storage.get('access_token');

    if (token && /\/api/.test(req.url)) {
      headers = headers.set('Authorization', `Bearer ${token}`);
      req = req.clone({ headers });
    }

    return next.handle(req);
  }
}
