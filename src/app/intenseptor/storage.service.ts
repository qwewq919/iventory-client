import { get, set, unset } from 'lodash';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class StorageService {
  key = 'access_token';

  constructor() {}

  public get(key: string, defaultValue: any = null): any {
    return localStorage.getItem(this.key);
  }

  public set(key: string, value: any) {
    const storage = (localStorage.getItem(this.key));
    set(storage, key, value);
    localStorage.setItem(this.key, storage);
  }

  public remove(key: string) {
    const storage = (localStorage.getItem(this.key));
    unset(storage, key);
    localStorage.setItem(this.key, storage);
  }

  public clear() {   
    localStorage.removeItem(this.key);
  }


}
