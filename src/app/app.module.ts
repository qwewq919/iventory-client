import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { HeaderComponent } from './shared/header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { AuthComponent } from './modals/auth/auth.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './intenseptor/auth.interceptor';
import { ItemsComponent } from './pages/items/items.component';
import { UsersComponent } from './pages/users/users.component';
import { CategoriesComponent } from './pages/categories/categories.component';
import { RolesComponent } from './pages/roles/roles.component';
import { AboutComponent } from './pages/about/about.component';
import { RoomsComponent } from './pages/rooms/rooms.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AddCategoryComponent } from './modals/category/add-category/add-category.component';
import { EdditCategoryComponent } from './modals/category/eddit-category/eddit-category.component';
import { AddItemComponent } from './modals/items/add-item/add-item.component';
import { EditComponent } from './modals/items/edit/edit.component';
import { AddRoomComponent } from './modals/room/add-room/add-room.component';
import { EdditRoomComponent } from './modals/room/eddit-room/eddit-room.component';
import { PersonalComponent } from './pages/personal/personal.component';
import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';
import { EditRolesComponent } from './modals/roles/edit-roles/edit-roles.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { QrcodeComponent } from './modals/items/qrcode/qrcode.component';
import { QrComponent } from './pages/qr/qr.component';
import { AddUserComponent } from './modals/user/add-user/add-user.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { PrintQRComponent } from './modals/items/print-qr/print-qr.component';
import { NgxPrintModule } from 'ngx-print';
import { ToastrModule } from 'ngx-toastr';
import { LogsComponent } from './pages/logs/logs.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { EditUserComponent } from './modals/user/edit-user/edit-user.component';
import { DeletComponent } from './modals/delet/delet.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    AuthComponent,
    ItemsComponent,
    UsersComponent,
    CategoriesComponent,
    RolesComponent,
    AboutComponent,
    RoomsComponent,
    EditComponent,
    AddItemComponent,
    AddRoomComponent,
    EdditRoomComponent,
    EdditCategoryComponent,
    AddCategoryComponent,
    PersonalComponent,
    EditRolesComponent,
    QrcodeComponent,
    QrComponent,
    AddUserComponent,
    PrintQRComponent,
    LogsComponent,
    EditUserComponent,
    DeletComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    ReactiveFormsModule,
    MatInputModule,
    FormsModule,
    MatDialogModule,
    MatButtonModule,
    MatIconModule,
    HttpClientModule,
    MatPaginatorModule,
    MatSelectModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    NgxPrintModule,
    MatSnackBarModule,
    ToastrModule.forRoot(),
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
