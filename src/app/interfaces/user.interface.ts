
export enum UserRole {
  User = 1,
  Admin = 2,
}

export class IUser {
  Roles: string;
  email: string;
  password: string;
  isActive: boolean;
  id: string;
  firstName: string;
  lastName: string;
  sub?: string;
}
