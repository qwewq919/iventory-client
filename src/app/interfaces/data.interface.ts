import { IUser } from './user.interface';

export class IData {
    pageIndex: number;
    pageSize: number;
}
