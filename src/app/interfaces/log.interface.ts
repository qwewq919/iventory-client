import { Data } from '@angular/router';

export class ILogs {
    date: Data;
    status: number;
    account?: {
        id: string;
        email: string;
        firstName: string;
        lastName: string;
    };
    item?: {
        id: string;
        name: string;

        category?: {
            name: string
        };
        room?: {
            name: string;
        };
    };
    recipient?: {
        firstName: string;
        lastName: string;
    };

}
