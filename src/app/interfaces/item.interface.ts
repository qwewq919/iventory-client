export class IItem {
    id?: number;
    name?: string;
    accountId?: string;
    roomId?: string;
    categoryId?: string;
    isActive?: boolean;
    room?: {
        id: string;
        name: string;
    };
    account?: {
        id: string;
        email: string;
        firstName: string;
        lastName: string;
    };
    category?: {
        id: string;
        name: string;
    };
    selected?: boolean;
}
